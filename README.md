# Automates Cellulaires

Projet en C

## Membres :
- Orlay Arnaud
- Zhou Nicolas


## Sujet :
Un automate cellulaire est un ensemble de cellules contenant chacune un état. Il évolue au cours du temps, ce qui change l'état des cellules. L'objectif est de créer un algorithme permettant d'afficher un automate cellulaire et les évolutions de celle-ci.


## Structure :
- conf: contient les fichiers de configurations pour un automate
- bin: Le fichier executable
- img: Les images générées par le code
- include: Les fichiers en-tête (.h)
- doc: La documentation (généré avec Doxygen)
- obj: Les fichiers objet (.o)
- src: Les fichiers source (.c)


- Makefile: Procedure de compilation
- Doxyfile: Paramètres de génaration de la documentation
- Sujet_automates_cellulaires.pdf: Le sujet du projet
- Rapport_automates_cellulaires.pdf: Le rapport du projet


## Lancement du programme :
- Compilation et execution du programme :
```bash
$ make
$ ./bin/main  <nom_fichier.ini>
```
- D'autres commandes pour executer le programme :

| Commande | Documentation |
| ------ | ------ |
| make run | affiche l'automate dans un terminal la regle 110 à deux états |
| make run_sierpinski | enregistre l'automate dans une image "the big Sierpinski" |
| make run_aleatoire | enregistre l'automate dans une image un automate avec une configuration initiale aléatoire |


## Documentation
Pour générer la documentation HTML et Latex :
```bash
$ doxygen Doxyfile
```
Une fois la documentation généré, ouvrir le fichier `./doc/html/index.html`.
Pour la documentation latex, exécuter les commandes suivantes :
```bash
$ cd ./doc/latex
$ make
```
Le `make` construit un fichier PDF sous le nom de "refman.pdf" accessible donc dans `./doc/latex/refman.pdf`.

La documentation en PDF est disponible depuis ce lien: [documentation PDF](/doc/latex/refman.pdf).