\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Index des structures de donn\IeC {\'e}es}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Structures de donn\IeC {\'e}es}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Index des fichiers}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Liste des fichiers}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Documentation des structures de donn\IeC {\'e}es}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}R\IeC {\'e}f\IeC {\'e}rence de la structure automate\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cell}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Description d\IeC {\'e}taill\IeC {\'e}e}{5}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Documentation des champs}{5}{subsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.2.1}f\_config}{6}{subsubsection.3.1.2.1}
\contentsline {subsubsection}{\numberline {3.1.2.2}regle}{6}{subsubsection.3.1.2.2}
\contentsline {subsubsection}{\numberline {3.1.2.3}tab}{6}{subsubsection.3.1.2.3}
\contentsline {section}{\numberline {3.2}R\IeC {\'e}f\IeC {\'e}rence de la structure configuration}{6}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Description d\IeC {\'e}taill\IeC {\'e}e}{6}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Documentation des champs}{7}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}etat\_centre}{7}{subsubsection.3.2.2.1}
\contentsline {subsubsection}{\numberline {3.2.2.2}etat\_droite}{7}{subsubsection.3.2.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.3}etat\_gauche}{7}{subsubsection.3.2.2.3}
\contentsline {subsubsection}{\numberline {3.2.2.4}etat\_sortie}{7}{subsubsection.3.2.2.4}
\contentsline {section}{\numberline {3.3}R\IeC {\'e}f\IeC {\'e}rence de la structure fichier\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}configuration}{7}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Description d\IeC {\'e}taill\IeC {\'e}e}{7}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Documentation des champs}{8}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}cellule\_symbole}{8}{subsubsection.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}emplacement\_initial}{8}{subsubsection.3.3.2.2}
\contentsline {subsubsection}{\numberline {3.3.2.3}enregistrer\_image}{8}{subsubsection.3.3.2.3}
\contentsline {subsubsection}{\numberline {3.3.2.4}hauteur}{8}{subsubsection.3.3.2.4}
\contentsline {subsubsection}{\numberline {3.3.2.5}largeur}{9}{subsubsection.3.3.2.5}
\contentsline {subsubsection}{\numberline {3.3.2.6}nb\_etat}{9}{subsubsection.3.3.2.6}
\contentsline {subsubsection}{\numberline {3.3.2.7}nb\_magique}{9}{subsubsection.3.3.2.7}
\contentsline {subsubsection}{\numberline {3.3.2.8}regle}{9}{subsubsection.3.3.2.8}
\contentsline {section}{\numberline {3.4}R\IeC {\'e}f\IeC {\'e}rence de la structure regle}{9}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Description d\IeC {\'e}taill\IeC {\'e}e}{9}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Documentation des champs}{10}{subsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.2.1}nombre\_etat}{10}{subsubsection.3.4.2.1}
\contentsline {subsubsection}{\numberline {3.4.2.2}numero\_binaire}{10}{subsubsection.3.4.2.2}
\contentsline {subsubsection}{\numberline {3.4.2.3}numero\_decimale}{10}{subsubsection.3.4.2.3}
\contentsline {subsubsection}{\numberline {3.4.2.4}tab\_configuration}{10}{subsubsection.3.4.2.4}
\contentsline {chapter}{\numberline {4}Documentation des fichiers}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}R\IeC {\'e}f\IeC {\'e}rence du fichier src/automate.c}{11}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Description d\IeC {\'e}taill\IeC {\'e}e}{12}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Documentation des fonctions}{12}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}\_affiche\_automate\_image()}{12}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}\_affiche\_automate\_terminal\_binaire()}{13}{subsubsection.4.1.2.2}
\contentsline {subsubsection}{\numberline {4.1.2.3}\_affiche\_automate\_terminal\_somme()}{14}{subsubsection.4.1.2.3}
\contentsline {subsubsection}{\numberline {4.1.2.4}affiche\_automate()}{15}{subsubsection.4.1.2.4}
\contentsline {subsubsection}{\numberline {4.1.2.5}creer\_automate()}{16}{subsubsection.4.1.2.5}
\contentsline {subsubsection}{\numberline {4.1.2.6}delete\_automate()}{17}{subsubsection.4.1.2.6}
\contentsline {subsubsection}{\numberline {4.1.2.7}evoluer()}{18}{subsubsection.4.1.2.7}
\contentsline {section}{\numberline {4.2}R\IeC {\'e}f\IeC {\'e}rence du fichier src/configuration.c}{19}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Description d\IeC {\'e}taill\IeC {\'e}e}{19}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Documentation des fonctions}{20}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}afficher\_configuration\_elementaire()}{20}{subsubsection.4.2.2.1}
\contentsline {subsubsection}{\numberline {4.2.2.2}afficher\_configuration\_multiple()}{21}{subsubsection.4.2.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.3}cmp\_conf()}{21}{subsubsection.4.2.2.3}
\contentsline {subsubsection}{\numberline {4.2.2.4}creer\_configuration()}{22}{subsubsection.4.2.2.4}
\contentsline {subsubsection}{\numberline {4.2.2.5}get\_etat\_centre()}{22}{subsubsection.4.2.2.5}
\contentsline {subsubsection}{\numberline {4.2.2.6}get\_etat\_droite()}{23}{subsubsection.4.2.2.6}
\contentsline {subsubsection}{\numberline {4.2.2.7}get\_etat\_gauche()}{23}{subsubsection.4.2.2.7}
\contentsline {subsubsection}{\numberline {4.2.2.8}get\_etat\_sortie()}{23}{subsubsection.4.2.2.8}
\contentsline {subsubsection}{\numberline {4.2.2.9}init\_configuration\_entree()}{24}{subsubsection.4.2.2.9}
\contentsline {subsubsection}{\numberline {4.2.2.10}init\_configuration\_sortie()}{25}{subsubsection.4.2.2.10}
\contentsline {section}{\numberline {4.3}R\IeC {\'e}f\IeC {\'e}rence du fichier src/fichier\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}configuration.c}{25}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Description d\IeC {\'e}taill\IeC {\'e}e}{26}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Documentation des fonctions}{26}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}afficher\_fichier\_configuration()}{26}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}creer\_fichier\_configuration()}{27}{subsubsection.4.3.2.2}
\contentsline {subsubsection}{\numberline {4.3.2.3}delete\_fichier\_configuration()}{28}{subsubsection.4.3.2.3}
\contentsline {subsubsection}{\numberline {4.3.2.4}get\_cellule\_symbole()}{28}{subsubsection.4.3.2.4}
\contentsline {subsubsection}{\numberline {4.3.2.5}get\_emplacement\_initial()}{29}{subsubsection.4.3.2.5}
\contentsline {subsubsection}{\numberline {4.3.2.6}get\_enregistrer\_image()}{29}{subsubsection.4.3.2.6}
\contentsline {subsubsection}{\numberline {4.3.2.7}get\_hauteur()}{30}{subsubsection.4.3.2.7}
\contentsline {subsubsection}{\numberline {4.3.2.8}get\_largeur()}{31}{subsubsection.4.3.2.8}
\contentsline {subsubsection}{\numberline {4.3.2.9}get\_nb\_etat()}{32}{subsubsection.4.3.2.9}
\contentsline {subsubsection}{\numberline {4.3.2.10}get\_nb\_magique()}{33}{subsubsection.4.3.2.10}
\contentsline {subsubsection}{\numberline {4.3.2.11}get\_regle()}{34}{subsubsection.4.3.2.11}
\contentsline {subsubsection}{\numberline {4.3.2.12}lecture\_fichier\_configuration()}{34}{subsubsection.4.3.2.12}
\contentsline {section}{\numberline {4.4}R\IeC {\'e}f\IeC {\'e}rence du fichier src/image.c}{35}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Description d\IeC {\'e}taill\IeC {\'e}e}{35}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Documentation des fonctions}{36}{subsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.2.1}creer\_image()}{36}{subsubsection.4.4.2.1}
\contentsline {subsubsection}{\numberline {4.4.2.2}ecrire\_donnees\_image\_ascii()}{37}{subsubsection.4.4.2.2}
\contentsline {subsubsection}{\numberline {4.4.2.3}ecrire\_donnees\_image\_binaire()}{38}{subsubsection.4.4.2.3}
\contentsline {subsubsection}{\numberline {4.4.2.4}ecrire\_entete\_image()}{39}{subsubsection.4.4.2.4}
\contentsline {section}{\numberline {4.5}R\IeC {\'e}f\IeC {\'e}rence du fichier src/main.c}{40}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Description d\IeC {\'e}taill\IeC {\'e}e}{40}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Documentation des fonctions}{41}{subsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.2.1}main()}{41}{subsubsection.4.5.2.1}
\contentsline {section}{\numberline {4.6}R\IeC {\'e}f\IeC {\'e}rence du fichier src/regle.c}{41}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Description d\IeC {\'e}taill\IeC {\'e}e}{42}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Documentation des fonctions}{43}{subsection.4.6.2}
\contentsline {subsubsection}{\numberline {4.6.2.1}\_config\_binaire()}{43}{subsubsection.4.6.2.1}
\contentsline {subsubsection}{\numberline {4.6.2.2}\_config\_somme()}{44}{subsubsection.4.6.2.2}
\contentsline {subsubsection}{\numberline {4.6.2.3}afficher\_regle()}{45}{subsubsection.4.6.2.3}
\contentsline {subsubsection}{\numberline {4.6.2.4}creer\_regle()}{46}{subsubsection.4.6.2.4}
\contentsline {subsubsection}{\numberline {4.6.2.5}creer\_tableau\_configuration()}{47}{subsubsection.4.6.2.5}
\contentsline {subsubsection}{\numberline {4.6.2.6}delete\_regle()}{48}{subsubsection.4.6.2.6}
\contentsline {subsubsection}{\numberline {4.6.2.7}get\_nombre\_etat()}{48}{subsubsection.4.6.2.7}
\contentsline {subsubsection}{\numberline {4.6.2.8}get\_numero\_binaire()}{49}{subsubsection.4.6.2.8}
\contentsline {subsubsection}{\numberline {4.6.2.9}get\_numero\_decimale()}{50}{subsubsection.4.6.2.9}
\contentsline {subsubsection}{\numberline {4.6.2.10}get\_tab\_configuration()}{50}{subsubsection.4.6.2.10}
\contentsline {subsubsection}{\numberline {4.6.2.11}recherche\_configuration()}{51}{subsubsection.4.6.2.11}
\contentsline {subsubsection}{\numberline {4.6.2.12}recherche\_indice()}{51}{subsubsection.4.6.2.12}
\contentsline {section}{\numberline {4.7}R\IeC {\'e}f\IeC {\'e}rence du fichier src/utils.c}{52}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Description d\IeC {\'e}taill\IeC {\'e}e}{53}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}Documentation des fonctions}{53}{subsection.4.7.2}
\contentsline {subsubsection}{\numberline {4.7.2.1}binary\_to\_decimal()}{53}{subsubsection.4.7.2.1}
\contentsline {subsubsection}{\numberline {4.7.2.2}decimal\_to\_binary()}{54}{subsubsection.4.7.2.2}
\contentsline {chapter}{Index}{55}{section*.82}
