/**
 * \file automate.c
 * \author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * \author Nicolas Zhou (nicolas.zhou@etu.univ-orleans.fr)
 * \brief Un automate cellulaire est une structure travaillant principalement sur l'évolution
 * de chaque cellule sur un certain nombre d'itération.
 * \version 0.1
 * \date 2019-10-04
 * 
 * @copyright Copyright (c) 2019
 * 
 **/


#include "automate.h"


//------------------------------------------------------------//
//                           Structure                        //
//------------------------------------------------------------//
/**
  * \struct automate_cell automate.h
  * \brief Un automate cellulaire
  * \param regle Numero de la regle
  * \param nb_ite Nombre d'iteration
  * \param taille La largeur maximale de l'automate
  * \param tab le tableau contenant les états de chaque iteration
  *
  * L'automate cellulaire contient le numero de la regle, le nombre d'iterations,
  * une taille en largeur et un tableau contenant les états de chaque iteration.
  **/
struct automate_cell{
  ptr_str_fc f_config;
  ptr_str_regle regle;
  int** tab;
};


//------------------------------------------------------------//
//                        Constructeur                        //
//------------------------------------------------------------//
/**
 * \brief Créer un automate appartir des dnnées de configuration (structure fichier_configuration)
 * 
 * \param f_config Un pointeur sur pointeur de structure fichier_configuration
 * \return ptr_automate Un pointeur sur un pointeur de structure automate_cell
 **/
ptr_automate creer_automate(ptr_str_fc f_config){
  // allouer de la memoire pour l'automate
  ptr_automate automate_c = (ptr_automate) malloc(sizeof(struct automate_cell));
  // tester si la memoire est bien allouee
  if(automate_c == NULL) {
     fprintf(stderr, "L'allocation memoire a echoue.");
     exit(EXIT_FAILURE);
  }
  automate_c->f_config = f_config;
  automate_c->regle = creer_regle(get_regle(f_config), get_nb_etat(f_config));
  char* init_cell = get_emplacement_initial(f_config);

  // allouer de la memoire pour le tableau des etats de chaque cellule
  int** tab = (int**) malloc(sizeof(int*) * (get_hauteur(f_config) + 1));
  if(tab == NULL) {
       fprintf(stderr, "L'allocation memoire a echoue.");
       exit(EXIT_FAILURE);
   }

  for (size_t i = 0; i < get_hauteur(f_config) + 1; i++) {
    tab[i] = (int*) malloc(sizeof(int) * get_largeur(f_config));
    if(tab[i] == NULL) {
       fprintf(stderr, "L'allocation memoire a echoue.");
       exit(EXIT_FAILURE);
    }

  }
  // stocker la configuration initiale
  for (size_t j = 0; j < get_largeur(automate_c->f_config); j++) {
    tab[0][j] = (int)(init_cell[j] - '0');
  }
  automate_c->tab = tab;
  return automate_c;
}


//------------------------------------------------------------//
//                       Autres Fonctions                     //
//------------------------------------------------------------//
/**
 * \brief Faire évoluer les ceellules de l'automate appartir d'une règle.
 * 
 * \param automate Un pointeur sur un pointeur de structure automate_cell
 **/
void evoluer(ptr_automate automate){
  ptr_str_conf conf = creer_configuration();
  // parcourir chaque ligne de l'automate
  for (size_t i = 0; i < get_hauteur(automate->f_config); i++) {
    // parcourir chaque cellule d'une ligne
    for (size_t j = 0; j < get_largeur(automate->f_config); j++) {
      unsigned int gauche;
      unsigned int centre;
      unsigned int droite;
      // stocke la valeur des cellules voisines et de la cellule actuelle
      centre = automate->tab[i][j];
      if (j == 0){
          gauche = automate->tab[i][get_largeur(automate->f_config)-1];
          droite = automate->tab[i][j+1];
      }
      else if(j == get_largeur(automate->f_config) - 1){
          gauche = automate->tab[i][j-1];
          droite = automate->tab[i][0];
      }
      else{
        gauche = automate->tab[i][j-1];
        droite = automate->tab[i][j+1];
      }
      // Automates cellulaires élémentaires
      // on initialise une configuration avec les cellules voisines
      // et la cellule actuelle,
      // puis on cherche la configuration qui corresponde a la configuration
      // creee afin de recuperer la valeur de sortie de la configuration
      if (get_nombre_etat(automate->regle) == 2){
        init_configuration_entree(conf, gauche, centre, droite);
        automate->tab[i+1][j] = recherche_configuration(automate->regle, conf);
      }
      // Etats supplementaires
      // on calcul la somme des voisins (cellule comprise) qui donne l'indice
      // que l'on va utiliser pour chercher dans le tableau de la regle
      else{
        unsigned int somme = gauche + centre + droite;
        automate->tab[i+1][j] = recherche_indice(automate->regle, somme);
      }
    }
  }
  free(conf);

}


//------------------------------------------------------------//
//                        Destruction                         //
//------------------------------------------------------------//
/**
 * \brief Désaloue la mémoire des attributs de la structure automate_cell, la structure
 * elle même et son pointeur.
 * 
 * \param ptr Un pointeur de pointeur de structure automate_cell
 **/
void delete_automate(ptr_automate* ptr){
  delete_regle(&((*ptr)->regle));
  for (size_t i = 0; i < get_hauteur((*ptr)->f_config) + 1; i++) {
    free((*ptr)->tab[i]);
  }
  free((*ptr)->tab);
  free((*ptr));
  *ptr = NULL;
}


//------------------------------------------------------------//
//                          Affichage                         //
//------------------------------------------------------------//
/**
 * \brief Affiche les attributs de la structure automate_cell
 * 
 * \param automate Un pointeur de pointeur de structure automate_cell
 **/
void _affiche_automate_terminal_somme(ptr_automate automate) {
  printf("Regle : %s\n", get_numero_binaire(automate->regle));
  printf("Nombre d'iterations : %d\n", get_hauteur(automate->f_config));
  for (size_t i = 0; i < get_hauteur(automate->f_config)+1; i++) {
    printf("|%2ld|", i);
    for (size_t j = 0; j < get_largeur(automate->f_config); j++) {
      if (automate->tab[i][j] == 0){
        printf(" ");
      }
      else{
        printf("%d", automate->tab[i][j]);
      }
    }
    printf("\n");
  }
}


/**
 * \brief Crée une mage PGM, stock le résultat de l'automate et enregistre l'image.
 * 
 * \param automate Un pointeur de pointeur de structure automate_cell
 **/
void _affiche_automate_image( ptr_automate automate ){
  FILE* fichier = creer_image( automate->f_config );
  ecrire_entete_image(fichier, automate->f_config);
  const unsigned int NOMBRE_MAGIQUE = get_nb_magique( automate->f_config );
  switch (NOMBRE_MAGIQUE){
    case 2:
      ecrire_donnees_image_ascii(fichier, automate->f_config, &automate->tab);
      break;

    case 5:
      ecrire_donnees_image_binaire(fichier, automate->f_config, &automate->tab);
      break;

    default:
      fprintf(stderr, "Le nombre magique est incorecte pour des images PGM.");
      exit( EXIT_FAILURE );
      break;
  }
  char* format;
  if (get_nb_magique(automate->f_config) == 5){
    format = "binaire";
  }
  else{
    format = "ascii";
  }
  printf("L'image PGM en %s \"regle_%s.pgm\" a été créé.\nLe fichier est enregistré dans le dossier img.\n", format, get_regle(automate->f_config));
  fclose( fichier );
}


/**
 * \brief Fonction générique perrmettant d'afficher dans le terminal avec des symboles
 * (ex: X, #, +, etc)
 * 
 * \param automate Un pointeur de pointeur de structure automate_cell
 **/
void _affiche_automate_terminal_binaire(ptr_automate automate) {
  printf("Regle : %u\n", get_numero_decimale(automate->regle));
  printf("Regle binaire : %s\n", get_numero_binaire(automate->regle));
  printf("Nombre d'iterations : %d\n", get_hauteur(automate->f_config));
  char symbole = get_cellule_symbole(automate->f_config);
  for (size_t i = 0; i < get_hauteur(automate->f_config)+1; i++) {
    printf("|%2ld|", i);
    for (size_t j = 0; j < get_largeur(automate->f_config); j++) {
      if (automate->tab[i][j] == 0){
        printf(" ");
      }
      else{
        printf("%c", symbole);
      }
    }
    printf("\n");
  }
}


/**
 * \brief Fonction generique permettant d'afficher differemment l'automate
 * 
 * \param automate Un pointeur de pointeur de structure automate_cell
 * \param _affiche Un pointeur de fonction pour le type d'affichage souhaité
 **/
void affiche_automate(ptr_automate automate, void(*_affiche)(ptr_automate)){
  (*_affiche)(automate);
}
