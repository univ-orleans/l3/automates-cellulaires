/**
 * \file fichier_configuration.c
 * \author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * \author Nicolas Zhou (nicolas.zhou@etu.univ-orleans.fr)
 * \brief Programmation des éléments de gestion de structure pour les fichiers de configuration.
 * \version 0.1
 * \date 2019-09-26
 *
 * \copyright Copyright (c) 2019
 *
 **/


#include "fichier_configuration.h"


//------------------------------------------------------------//
//                           Structure                        //
//------------------------------------------------------------//
/**
 * \struct fichier_configuration fichier_configuration.h
 * \brief Une structure pour stocker les paramètres des fichiers de configuration des automates cellulaire.
 * \param regle La règle (au format binaire) permétant l'évolution des cellules de l'automate.
 * \param nb_etat Le nombre d'état possible pour une cellule.
 * \param hauteur La hauteur de l'automate.
 * \param largeur La largeur de l'automate.
 * \param cellule_symbole Le symbole utilisé pour représenté une cellule (vivante) dans l'affichage terminal.
 * \param emplacement_initial Une chaine de caractère permettant l'initilisation de l'emplacement des cellules vivnates.
 * \param enregistrer_image Un boolean permattant d'identifier le type d'affichage (terminal ou image).
 * \param nb_magique LE nombre maagique identifie le format d'enregistrement de l'image (ASCII ou Binaire).
 **/
struct fichier_configuration {
  char* regle;
  unsigned int nb_etat;
  unsigned int hauteur;
  unsigned int largeur;
  char cellule_symbole;
  char* emplacement_initial;
  bool enregistrer_image;
  unsigned int nb_magique;
};


//------------------------------------------------------------//
//                        Constructeur                        //
//------------------------------------------------------------//
/**
 * \brief Créer un pointeur de pointeur de structure fichier_configuration à partir du nom du fichier que
 * l'éxecutable prend en paramètre.
 *
 * \param nom_fichier Le chemin relatif du fichier de configuration que le programme doit traiter.
 * \return ptr_str_fc Un pointeur sur un pointeur de structure fichier_configuration.
 **/
ptr_str_fc creer_fichier_configuration( const char* nom_fichier ){

  // Création d'un fichier
  FILE* fichier = fopen( nom_fichier, "r" );
  if( fichier == NULL ){
    fprintf(stderr, "Erreur d'ouverture du fichier %s\n", nom_fichier);
    exit(EXIT_FAILURE );
  }

  // Création d'une structure fichier_configuration
  ptr_str_fc f_config = (ptr_str_fc)malloc( sizeof(struct fichier_configuration) );
  if( f_config == NULL ){
    fprintf(stderr, "Erreur de création d'une structure fichier_configuration\n");
    exit(EXIT_FAILURE );
  }
  f_config->emplacement_initial = NULL;
  f_config->regle = NULL;

  // Lecture des paramètres du fichier de configuration
  lecture_fichier_configuration(fichier, f_config);

  fclose( fichier );
  return f_config;
}


//------------------------------------------------------------//
//                         Accesseurs                         //
//------------------------------------------------------------//
/**
 * \brief Retourne l'attribut "regle".
 *
 * \param f_config Un pointeur de pointeur de structure fichier_configuration.
 * \return unsigned int
 **/
char* get_regle( ptr_str_fc f_config ){
  return f_config->regle;
}


/**
 * \brief Retourne l'attribut "nb_etat".
 *
 * \param f_config Un pointeur de pointeur de structure fichier_configuration.
 * \return unsigned int
 **/
unsigned int get_nb_etat( ptr_str_fc f_config ){
  return f_config->nb_etat;
}


/**
 * \brief Retourne l'attribut "hauteur".
 *
 * \param f_config Un pointeur de pointeur de structure fichier_configuration.
 * \return unsigned int
 **/
unsigned int get_hauteur( ptr_str_fc f_config ){
  return f_config->hauteur;
}


/**
 * \brief Retourne l'attribut "largeur".
 *
 * \param f_config Un pointeur de pointeur de structure fichier_configuration.
 * \return unsigned int
 **/
unsigned int get_largeur( ptr_str_fc f_config ){
  return f_config->largeur;
}


/**
 * \brief Retourne l'attribut "cellule_symbole".
 *
 * \param f_config Un pointeur de pointeur de structure fichier_configuration.
 * \return char
 **/
char get_cellule_symbole( ptr_str_fc f_config ){
  return f_config->cellule_symbole;
}


/**
 * \brief Retourne l'attribut "emplacement_initial".
 *
 * \param f_config Un pointeur de pointeur de structure fichier_configuration.
 * \return char* Une chaine de caractère représentant les emplacement des cellules initiales.
 **/
char* get_emplacement_initial( ptr_str_fc f_config ){
  return f_config->emplacement_initial;
}


/**
 * \brief Retourne l'attribut "enregistrer_image".
 *
 * \param f_config Un pointeur de pointeur de structure fichier_configuration.
 * \return true Si l'utilisateur souhaite retourner une image PGM.
 * \return false Si l'utilisateur souhaite utiliser l'affichage du terminal.
 **/
bool get_enregistrer_image( ptr_str_fc f_config ){
  return f_config->enregistrer_image;
}


/**
 * \brief Retourne l'attribut "nb_magique".
 *
 * \param f_config Un pointeur de pointeur de structure fichier_configuration.
 * \return unsigned int Le nombre magique.
 **/
unsigned int get_nb_magique( ptr_str_fc f_config ){
  return f_config->nb_magique;
}


//------------------------------------------------------------//
//                       Autres Fonctions                     //
//------------------------------------------------------------//
/**
 * \brief Lire les valeurs des paramètres du fichier de configuration (ASCII) passé en paramètre.
 *
 * \param fichier Le fichier de configuration.
 * \param f_config Le pointeur de pointeur de structure fichier_configuration.
 **/
void lecture_fichier_configuration(FILE* fichier, ptr_str_fc f_config){

  enum etats { ENREGISTRER_IMAGE, NOMBRE_ETAT, REGLE, HAUTEUR, LARGEUR, CELLULE_SYMBOLE, EMPLACEMENT_INITIAL, NOMBRE_MAGIQUE };
  enum etats etat = ENREGISTRER_IMAGE;
  const int MAX_LENGTH = 2000;

  char* buf = (char*)malloc( sizeof(char) * MAX_LENGTH );
  if(buf == NULL) {
     fprintf(stderr, "L'allocation memoire a echoue.");
     exit(EXIT_FAILURE);
  }
  bool lire_fichier = true;
  char* enregistrer_str;
  char* emp_initial;
  char* regle_tmp;
  int nombre_etat_tmp;
  int hauteur_tmp;
  int largeur_tmp;

  while( lire_fichier ){
    if( fgets(buf, MAX_LENGTH, fichier) == NULL ){
      lire_fichier = false;
    }
    while( strncmp(buf, "#", 1) == 0 || strncmp(buf, "\n", 1) == 0 ){
      fgets(buf, MAX_LENGTH, fichier);
    }

    switch( etat ){

      // Récupération de la valeur "YES" ou "NO" pour savoir si 
      // l'utilisateur souhaite enregistrer son automate en image ou non.
      case ENREGISTRER_IMAGE:
      enregistrer_str = (char*)malloc( sizeof(char) * 3 + 1);
      if( enregistrer_str == NULL ){
        fprintf(stderr, "Erreur d'initialisation du tableau pour l'enregistrement de l'image \n");
        exit(EXIT_FAILURE);
      }
      if(sscanf(buf, "ENREGISTREMENT = %s", enregistrer_str) == 1){
        if( strcmp(enregistrer_str, "YES") == 0 ) f_config->enregistrer_image = true;
        else if( strcmp(enregistrer_str, "NO") == 0 ) f_config->enregistrer_image = false;
        else{
          fprintf(stderr, "FICHIER CONFIG: le paramètre 'ENREGISTREMENT' n'est pas valide.\n");
          exit(EXIT_FAILURE);
        }
        etat = NOMBRE_ETAT;
      }
      else {
        fprintf(stderr, "FICHIER CONFIG: le paramètre 'ENREGISTREMENT' n'est pas renseigné\n");
        exit(EXIT_FAILURE);
      }
      free( enregistrer_str );
      enregistrer_str = NULL;
      break;

      // On récupère le nombre d'état possible
      case NOMBRE_ETAT:
      if(sscanf(buf, "NOMBRE_ETATS = %d", &nombre_etat_tmp) == 1){
        if (nombre_etat_tmp >= 2){
          f_config->nb_etat = nombre_etat_tmp;
          etat = REGLE;
        }
        else{
          fprintf(stderr, "FICHIER CONFIG: le paramètre 'NOMBRE_ETATS' n'est pas valide.\nIl faut minimum deux etats.\n");
          exit(EXIT_FAILURE);
        }
      }
      else {
        fprintf(stderr, "FICHIER CONFIG: le paramètre 'NOMBRE_ETATS' n'est pas renseigné\n");
        exit(EXIT_FAILURE);
      }
      break;

      // Récupération de la règle en chaine de caractère
      case REGLE:
      if (f_config->nb_etat == 2){
        regle_tmp = (char*)malloc(sizeof(char) * 8 + 1);
      }
      else{
        regle_tmp = (char*)malloc(sizeof(char) * ((f_config->nb_etat - 1) * 3 + 1) + 1);
      }
      if( regle_tmp == NULL ){
        fprintf(stderr, "Erreur d'initialisation du tableau pour la regle\n");
        exit(EXIT_FAILURE);
      }
      if(sscanf(buf, "REGLE = %s", regle_tmp) == 1){
        // tester s'il y a bien le bon nombre de chiffres
        if(f_config->nb_etat == 2 && strlen(regle_tmp) != 8){
          fprintf(stderr, "Erreur d'initialisation de la regle\nPour deux etats, la regle doit etre ecrit en binaire sous forme d'un octet.\n");
          exit(EXIT_FAILURE);
        }
        else if(f_config->nb_etat > 2 && strlen(regle_tmp) != (f_config->nb_etat - 1) * 3 + 1){
          fprintf(stderr, "Erreur d'initialisation de la regle\nPour %d etats, la regle doit avoir %d chiffres.\n", f_config->nb_etat , (f_config->nb_etat - 1) * 3 + 1);
          exit(EXIT_FAILURE);
        }
        // tester si les chiffres entres sont correctes
        for (size_t i = 0; i < strlen(regle_tmp); i++) {
          if ((unsigned int)(regle_tmp[i] - '0') >= f_config->nb_etat){
              fprintf(stderr, "Erreur de saisie de la regle\nPour %d etats, les chiffres doivent etre compris entre 0 et %d.\n", f_config->nb_etat , f_config->nb_etat - 1);
              exit(EXIT_FAILURE);
          }
        }
        f_config->regle = regle_tmp;
        regle_tmp = NULL;
        etat = HAUTEUR;
      }
      else {
        fprintf(stderr, "FICHIER CONFIG: le paramètre 'REGLE' n'est pas renseigné\n");
        exit(EXIT_FAILURE);
      }
      break;

      // Lecture ded la hauteur de l'automate
      case HAUTEUR:
      if(sscanf(buf, "HAUTEUR = %d", &hauteur_tmp) == 1){
        if ( hauteur_tmp > 1){
          f_config->hauteur = hauteur_tmp;
          etat = LARGEUR;
        }
        else{
          fprintf(stderr, "Erreur d'initialisation de l'hauteur\nVeuillez indiquer un entier positif.\n");
          exit(EXIT_FAILURE);
        }
      }
      else {
        fprintf(stderr, "FICHIER CONFIG: le paramètre 'HAUTEUR' n'est pas renseigné\n");
        exit(EXIT_FAILURE);
      }
      break;

      // Récupération de la largeur
      case LARGEUR:
      if(sscanf(buf, "LARGEUR = %d", &largeur_tmp) == 1){
        if ( largeur_tmp > 1){
          f_config->largeur = largeur_tmp;
          etat = LARGEUR;
        }
        else{
          fprintf(stderr, "Erreur d'initialisation de la largeur\nVeuillez indiquer un entier positif.\n");
          exit(EXIT_FAILURE);
        }
        etat = CELLULE_SYMBOLE;
      }
      else {
        fprintf(stderr, "FICHIER CONFIG: le paramètre 'LARGEUR' n'est pas renseigné\n");
        exit(EXIT_FAILURE);
      }
      break;

      // Lecture du caractère utilisé pour représenter une cellule dite "vivante"
      case CELLULE_SYMBOLE:
        if(sscanf(buf, "CELLULE_SYMBOLE = %c", &f_config->cellule_symbole) == 1) etat = EMPLACEMENT_INITIAL;
        else if( !f_config->enregistrer_image ){
          fprintf(stderr, "FICHIER CONFIG: le paramètre 'CELLULE_SYMBOLE' n'est pas renseigné\n");
          exit(EXIT_FAILURE);
        }
      break;

      // Lecture de la ligne d'initialiation
      case EMPLACEMENT_INITIAL:
      emp_initial = (char*)malloc(f_config->largeur * sizeof(char) + 1);
      if( emp_initial == NULL ){
        fprintf(stderr, "Erreur d'initialisation du tableau pour les emplacements initials\n");
        exit(EXIT_FAILURE);
      }
      if(sscanf(buf, "EMPLACEMENT_INITIAL = %s", emp_initial) == 1){
        // Initialiser aleatoirement les cellules de l'automate
        if (strcmp(emp_initial,  "ALEATOIRE") == 0) {
          for (size_t i = 0; i < f_config->largeur; i++) {
            emp_initial[i] = (char)(rand() % f_config->nb_etat) + '0';
          }
        }
        else if( strlen( emp_initial ) != f_config->largeur ){
          fprintf(stderr, "La chaîne %s doit contenir %d chiffres.\n", emp_initial, f_config->largeur);
          exit(EXIT_FAILURE);
        }
        else{
          for (size_t i = 0; i < f_config->largeur; i++) {
            if (emp_initial[i] - '0' > (int)f_config->nb_etat){
              fprintf(stderr, "La chaîne %s doit contenir des chiffres entre 0 et %u.\n", emp_initial, f_config->nb_etat);
              exit(EXIT_FAILURE);
            }
          }
        }

        f_config->emplacement_initial = emp_initial;
        emp_initial = NULL;
        etat = NOMBRE_MAGIQUE;
      }
      else {
        fprintf(stderr, "FICHIER CONFIG: le paramètre 'EMPLACEMENT_INITIAL' n'est pas renseigné\n");
        exit(EXIT_FAILURE);
      }
      break;

      // Lecture du nombre magique qui détermine le format d'enregistrement
      // de l'image (ASCII ou Binaire)
      case NOMBRE_MAGIQUE:
      if( sscanf(buf, "NOMBRE_MAGIQUE = P%ud", &f_config->nb_magique) == 0 && !f_config->enregistrer_image ){
        fprintf(stderr, "FICHIER CONFIG: le paramètre 'NOMBRE_MAGIQUE' n'est pas renseigné\n");
        exit(EXIT_FAILURE);
      }
      lire_fichier = false;
      break;
    }
  }
  free( buf );
  buf = NULL;
}


//------------------------------------------------------------//
//                         Suppression                        //
//------------------------------------------------------------//
/**
 * \brief Déalou la mémoire utilisé par la structure fichier_configuration ainsi que ses attributs.
 *
 * \param f_config Un pointeur de pointeur de structure fichier_configuration.
 **/
void delete_fichier_configuration( ptr_str_fc f_config ){
  free( f_config->emplacement_initial );
  if (f_config->nb_etat > 2)
    free( f_config->regle );
  free( f_config );
  f_config = NULL;
}


//------------------------------------------------------------//
//                          Affichage                         //
//------------------------------------------------------------//
/**
 * \brief Affiche les attributs de la structure fichier_configuration
 *
 * \param f_config Le pointeur de pointeur de structure fichier_configuration.
 */
void afficher_fichier_configuration( ptr_str_fc f_config ){
  printf("enregistrer image : %d\n", f_config->enregistrer_image);
  printf("regle : %s\n", f_config->regle);
  printf("nombre etats : %u\n", f_config->nb_etat);
  printf("hauteur : %u\n", f_config->hauteur);
  printf("largeur : %u\n", f_config->largeur);
  printf("cellule symbole : %c\n", f_config->cellule_symbole);
  printf("emplacement initial : %s\n", f_config->emplacement_initial);
  printf("nombre magique : %u\n", f_config->nb_magique);
}
