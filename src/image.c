/**
 * \file image.c
 * \author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * \author Nicolas Zhou (nicolas.zhou@etu.univ-orleans.fr)
 * \brief Bibliotheque de création d'image PGM
 * \version 0.1
 * \date 2019-09-26
 * 
 * \copyright Copyright (c) 2019
 **/


#include "image.h"


//------------------------------------------------------------//
//                        Constructeur                        //
//------------------------------------------------------------//
/**
 * \brief Crée un fichier image au format (.pgm) ainsi qu'un pointeur de fichier.
 * 
 * \param f_config Un pointeur sur un pointeur de structure fichier_configuration.
 * \return FILE* Le fichier image à traiter.
 **/
FILE* creer_image( ptr_str_fc f_config ){
    char* regle = (char*)get_regle( f_config );

    char* nom_fichier = (char*)malloc( sizeof(char) * (strlen(regle) + 15) );
    if( nom_fichier == NULL ){
        fprintf(stderr, "Erreur d'allocation de la place mémoire pour le nom du fichier image.\n");
        exit( EXIT_FAILURE );
    }
    strcpy( nom_fichier, DOSSIER_IMAGE);
    strcat( nom_fichier, "regle_");
    strcat( nom_fichier, regle);
    strcat( nom_fichier, FORMAT_IMAGE);

    FILE* fichier = get_enregistrer_image(f_config) ? fopen( nom_fichier, "wb" ) : fopen( nom_fichier, "wt" );
    if( fichier == NULL ){
        fprintf(stderr, "Erreur de création de fichier image.\n");
        exit( EXIT_FAILURE );
    }
    free( nom_fichier );
    return fichier;
}


//------------------------------------------------------------//
//                       Autres Fonctions                     //
//------------------------------------------------------------//
/**
 * \brief Ecrit dans l'en-tête de l'image passé en paramètres les éléments définit au préalable 
 * dans le fichier de configuration.
 * 
 * \param fichier L'image que l'ont souhaite obtenir.
 * \param f_config Un pointeur sur un pointeur de structure fichier_configuration.
 **/
void ecrire_entete_image( FILE* fichier, ptr_str_fc f_config ){
    const unsigned int NOMBRE_MAGIQUE = get_nb_magique(f_config);

    // Si le fichier est en ASCII ou en binaire
    if( (NOMBRE_MAGIQUE == 2) || (NOMBRE_MAGIQUE == 5) ){
        const char* FORMAT = NOMBRE_MAGIQUE == 2 ? "ASCII" : "Binaire";
        fprintf(fichier, "P%d\n", NOMBRE_MAGIQUE);
        fprintf(fichier, "# AUTOMATE CELLULAIRE: regle %s et format %s\n", get_regle(f_config), FORMAT);
        fprintf(fichier, "%d %d\n", get_largeur(f_config), get_hauteur(f_config));
        if( NOMBRE_MAGIQUE == 2 ) fprintf(fichier, "%d\n", get_nb_etat(f_config)-1);
        else fprintf(fichier, "%d\n", 255);
    }
    else{
        printf("Le nombre magique est incorrecte.\n");
        exit( EXIT_FAILURE );
    }
}


/**
 * \brief Ecrit les données d'une image PGM dans un format ASCII (nombre magique P2).
 * 
 * \param fichier L'image que l'ont souhaite obtenir.
 * \param f_config Un pointeur sur un pointeur de structure fichier_configuration.
 * \param tableau Les données de l'image.
 **/
void ecrire_donnees_image_ascii( FILE* fichier, ptr_str_fc f_config, int*** tableau){
    for(unsigned int i = 0; i < get_hauteur(f_config); i++){
        for(unsigned int j = 0; j < get_largeur(f_config); j++){
            fprintf(fichier, "%d  ", (*tableau)[i][j]);
        }
        fprintf(fichier, "\n");
    }
}


/**
 * \brief Ecrit les données d'une image PGM dans un format binaire (nombre magique P5)
 * 
 * \param fichier L'image que l'ont souhaite obtenir.
 * \param f_config Un pointeur sur un pointeur de structure fichier_configuration.
 * \param tableau Les données de l'image.
 **/
void ecrire_donnees_image_binaire( FILE* fichier, ptr_str_fc f_config, int*** tableau){
    for(unsigned int i = 0; i < get_hauteur(f_config); i++){
        for(unsigned int j = 0; j < get_largeur(f_config); j++){
            (*tableau)[i][j] = ((*tableau)[i][j] * 255) / (get_nb_etat(f_config) - 1);
            fwrite(&((*tableau)[i][j]), sizeof(char), 1, fichier);
        }
    }
}


