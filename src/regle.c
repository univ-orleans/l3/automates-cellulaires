/**
 * \file regle.c
 * \author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * \author Nicolas Zhou (nicolas.zhou@etu.univ-orleans.fr)
 * \brief Une regle permet de définir quelles configurations permettrons de défiir les cellules suivantes.
 * \version 0.1
 * \date 2019-10-04
 * 
 * @copyright Copyright (c) 2019
 * 
 **/


#include "regle.h"


//------------------------------------------------------------//
//                           Structure                        //
//------------------------------------------------------------//
/**
  * \struct regle regle.h
  * \brief Une regle
  * \param nombre_etat Le nombre d'état possible pour une cellule
  * \param numero_decimale Le numero associé à l'une des regles possible
  * \param numero_binaire Le numero de la regle converti en binaire
  * \param tab_configuration Un tableau de configuration
  *
  * Une regle s'identifie par un numero qui définit lequel ou lesquels de ses
  * configurations sera utilisé pour déterminer la cellule suivante.
  **/
struct regle {
  unsigned int nombre_etat;
  unsigned int numero_decimale;
  char* numero_binaire;
  ptr_str_conf* tab_configuration;
};


//------------------------------------------------------------//
//                        Constructeur                        //
//------------------------------------------------------------//
/**
  * \brief Créer un tableau de configuration pour deux etats
  * 
  * \param numero_binaire Une chaine de caractère représentant le numéro de la regle en binaire
  * \param nombre_etat Le nombre d'état possible pour une cellule
  * \param ptr un pointeur sur un pointeur de struct regle
  **/
void _config_binaire(char* numero_binaire, unsigned int nombre_etat, ptr_str_regle* ptr){
  ptr_str_conf* tab = (ptr_str_conf*)malloc( pow(nombre_etat, 3) * sizeof(struct configuration*) );
  if(tab == NULL) {
     fprintf(stderr, "L'allocation memoire a echoue.");
     exit(EXIT_FAILURE);
  }
  unsigned int cpt = 0;
  char* temp = (char*)malloc(sizeof(char) * 8);
  if(temp == NULL) {
     fprintf(stderr, "L'allocation memoire a echoue.");
     exit(EXIT_FAILURE);
  }
  // initialiser les configurations en fonction de la regle donnee
  for(int i = pow(nombre_etat, 3) - 1; i >= 0; i-- ){
    decimal_to_binary(&temp, i);
    tab[cpt] = creer_configuration();
    init_configuration_entree(tab[cpt], (int)temp[5]-'0', (int)temp[6]-'0', (int)temp[7]-'0');
    init_configuration_sortie(tab[cpt], (int)(numero_binaire[cpt]-'0'));
    cpt++;
  }
  free(temp);
  (*ptr)->tab_configuration = tab;
}


/**
 * \brief Créer un tableau de configuration avec plus de 3 etats
 * 
 * \param numero_binaire Une chaine de caractère représentant le numéro de la regle en binaire
 * \param nombre_etat Le nombre d'état possible pour une cellule
 * \param ptr Un pointeur sur un pointeur de structure regle
 **/
void _config_somme(char* numero_binaire, unsigned int nombre_etat, ptr_str_regle* ptr){
  ptr_str_conf* tab = (ptr_str_conf*)malloc( ((nombre_etat - 1) * 3 + 1) * sizeof(struct configuration*) );
  if(tab == NULL) {
     fprintf(stderr, "L'allocation memoire a echoue.");
     exit(EXIT_FAILURE);
  }
  unsigned int cpt = 0;
  // initialisation des configurations en fonction de la regle donnee
  for(int i = ((nombre_etat - 1) * 3 + 1) - 1; i >= 0; i-- ){
    tab[cpt] = creer_configuration();
    init_configuration_sortie(tab[cpt], (int)numero_binaire[cpt]-'0');
    cpt++;
  }
  (*ptr)->tab_configuration = tab;
}


/**
 * \brief Créer une structure regle
 * 
 * \param numero Le numero de la regle en binaire à appliquer.
 * \param nombre_etat Le nombre d'état possible pour une cellule.
 * \return ptr_str_regle Un pointeur sur un pointeur de structure regle.
 **/
ptr_str_regle creer_regle(char* numero, unsigned int nombre_etat){
  ptr_str_regle ptr = (ptr_str_regle)malloc( sizeof(struct regle) );
  if(ptr == NULL) {
     fprintf(stderr, "L'allocation memoire a echoue.");
     exit(EXIT_FAILURE);
  }
  ptr->nombre_etat = nombre_etat;
  ptr->numero_decimale = binary_to_decimal(numero);
  ptr->numero_binaire = numero;
  // la regle est creee en fonction du nombre d'etats de l'automate
  // s'il y en a 2 alors, il va creer les configurations avec la fonction _config_binaire
  if(nombre_etat == 2){
    creer_tableau_configuration(ptr->numero_binaire, nombre_etat, ptr, &_config_binaire);
  }
  // sinon la fonction _config_somme sera utilisee
  else{
    creer_tableau_configuration(ptr->numero_binaire, nombre_etat, ptr, &_config_somme);
  }
  return ptr;
}


/**
 * \brief Créer un tbleau de configuration en fonction du nombre d'état possible
 * 
 * \param numero_binaire Une chaine de caractère représentant le numéro de la regle en binaire
 * \param nombre_etat Le nombre d'état possible pour une cellule
 * \param ptr Un pointeur sur un pointeur de structure regle
 * \param _config La configuration choisie
 **/
void creer_tableau_configuration(char* numero_binaire, unsigned int nombre_etat, ptr_str_regle ptr, void(*_config)(char*, unsigned int, ptr_str_regle*)){
  (*_config)( numero_binaire, nombre_etat, &ptr);
}

//------------------------------------------------------------//
//                       Autres Fonctions                     //
//------------------------------------------------------------//
/**
 * \brief Trouve la valeur de la sortie de la configuration passé en paramètre à partir d'une configuration identique présente dans la regle.
 * 
 * \param regle Un pointeur sur un pointeur de structure regle
 * \param conf Un pointeur sur un pointeur de structure configuration
 * \return int La sortie de la configuration
 **/
int recherche_configuration(ptr_str_regle regle, ptr_str_conf conf){
  for (size_t i = 0; i < pow(regle->nombre_etat, 3); i++) {
    if (cmp_conf(regle->tab_configuration[i], conf)){
      return get_etat_sortie(regle->tab_configuration[i]);
    }
  }
  printf("Erreur lors de la recherche de la configuration.\n");
  exit(0);
}


/**
 * \brief Trouve la valeur de la sortie a partir de l'indice donnee en parametre
 * 
 * \param regle Un pointeur sur un pointeur de structure regle.
 * \param indice Un entier positif
 * \return int La sortie de la configuration
 **/
int recherche_indice(ptr_str_regle regle, unsigned int indice){
  return get_etat_sortie(regle->tab_configuration[indice]);
}


//------------------------------------------------------------//
//                           Accesseurs                       //
//------------------------------------------------------------//
/**
 * \brief Retourne l'attribut "nombre_etat".
 * 
 * \param ptr Un pointeur sur un pointeur de structure regle
 * \return unsigned int 
 **/
unsigned int get_nombre_etat( ptr_str_regle ptr ){
  return ptr->nombre_etat;
}


/**
 * \brief Retourne l'attribut "numero_decimale".
 * 
 * \param ptr Un pointeur sur un pointeur de structure regle
 * \return unsigned int 
 **/
unsigned int get_numero_decimale( ptr_str_regle ptr ){
  return ptr->numero_decimale;
}


/**
 * \brief Retourne l'attribut "numero_binaire".
 * 
 * \param ptr Un pointeur sur un pointeur de structure regle
 * \return char* 
 **/
char* get_numero_binaire( ptr_str_regle ptr ){
  return ptr->numero_binaire;
}


/**
 * \brief Retourne l'attribut tab configuration.
 * 
 * \param ptr Un pointeur sur un pointeur de structure regle
 * \return ptr_str_conf* 
 **/
ptr_str_conf* get_tab_configuration( ptr_str_regle ptr ){
  return ptr->tab_configuration;
}

//------------------------------------------------------------//
//                        Destruction                         //
//------------------------------------------------------------//
/**
 * \brief Desallouer la memoire de la regle
 * 
 * \param ptr Un pointeur sur un pointeur de structure regle
 **/
void delete_regle(ptr_str_regle* ptr){
  unsigned int taille_tab;
  if ((*ptr)->nombre_etat == 2){
    taille_tab = pow((*ptr)->nombre_etat, 3);
  }
  else{
    taille_tab = ((*ptr)->nombre_etat - 1) * 3 + 1;
  }
  for (size_t i = 0; i < taille_tab; i++) {
    free((*ptr)->tab_configuration[i]);
  }
  free((*ptr)->tab_configuration);
  if ((*ptr)->nombre_etat == 2){
    free((*ptr)->numero_binaire);
  }
  free((*ptr));
  *ptr = NULL;
}


//------------------------------------------------------------//
//                          Affichage                         //
//------------------------------------------------------------//
/**
 * \brief Affiche une regle (l'ensemble des configurations)
 * 
 * \param ptr Un pointeur sur un pointeur de structure regle
 **/
void afficher_regle( ptr_str_regle ptr ){
  unsigned int taille_tab;
  // affichage pour 2 etats
  if (ptr->nombre_etat == 2){
    taille_tab = pow(ptr->nombre_etat, 3);
    for(unsigned int i = 0; i < taille_tab; i++){
      afficher_configuration_elementaire(ptr->tab_configuration[i]);
    }
  }
  // affichage pour 3 etats ou plus
  else{
    taille_tab = (ptr->nombre_etat - 1) * 3 + 1;
    for(unsigned int i = 0; i < taille_tab; i++){
      afficher_configuration_multiple(ptr->tab_configuration[i], i);
    }
  }

}
