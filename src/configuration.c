/**
 * \file configuration.c
 * \author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * \author Nicolas Zhou (nicolas.zhou@etu.univ-orleans.fr)
 * \brief Une configuration est une structure permettant de définir l'état de la 
 * cellule suivante en fonction d'une regle.
 * \version 0.1
 * \date 2019-10-04
 * 
 * @copyright Copyright (c) 2019
 * 
 **/


#include "configuration.h"


//------------------------------------------------------------//
//                           Structure                        //
//------------------------------------------------------------//
/**
  * \struct configuration configuration.h
  * \brief Une configuration
  * \param etat_gauche L'état de la céllule voisine (celle de gauche)
  * \param etat_centre L'état de la céllule actuelle (celle au centre)
  * \param etat_droite L'état de la céllule voisine (celle de droite)
  * \param etat_sortie L'état de la prochaine cellule
  *
  * Une configuration est donc en ensemble d'état contenant celui de la cellule suivante 
  * (celui à déterminer), la cellule actuelle ainsi que ses deux voisons de gauche et droite.
  **/
struct configuration {
  unsigned int etat_gauche;
  unsigned int etat_centre;
  unsigned int etat_droite;
  unsigned int etat_sortie;
};


//------------------------------------------------------------//
//                        Constructeur                        //
//------------------------------------------------------------//
/**
 * \brief Créer une structure configuration en initialisant ses valeurs à 0.
 * 
 * \return ptr_str_conf Un pointeur sur une pointeur de structure configuration
 **/
ptr_str_conf creer_configuration(){
  ptr_str_conf ptr = (ptr_str_conf)malloc( sizeof(struct configuration) );
  if(ptr == NULL) {
     fprintf(stderr, "L'allocation memoire a echoue.");
     exit(EXIT_FAILURE);
  }
  ptr->etat_gauche = 0;
  ptr->etat_centre = 0;
  ptr->etat_droite = 0;
  ptr->etat_sortie = 0;
  return ptr;
}


//------------------------------------------------------------//
//                        Initialisation                      //
//------------------------------------------------------------//
/**
 * \brief Initialise les valeurs pour une structure configuration donnée
 * 
 * \param ptr Un pointeur sur un pointeur de structure
 * \param gauche La valeur de l'état de la cellule de gauche
 * \param centre La valeur de l'état de la cellule de du milieur
 * \param droite La valeur de l'état de la cellule de droite
 **/
void init_configuration_entree(ptr_str_conf ptr, unsigned int gauche, unsigned int centre, unsigned int droite){
  ptr->etat_gauche = gauche;
  ptr->etat_centre = centre;
  ptr->etat_droite = droite;
}


/**
 * \brief Initialise la valeurs de sortie pour une structure configuration donnée.
 * 
 * \param ptr Un pointeur sur un pointeur de structure
 * \param sortie La valeur de l'état de la cellule suivante
 **/
void init_configuration_sortie(ptr_str_conf ptr, unsigned int sortie){
  ptr->etat_sortie = sortie;
}


//------------------------------------------------------------//
//                         Accesseurs                         //
//------------------------------------------------------------//
/**
 * \brief Retourne l'attribut etat_gauche.
 * 
 * \param ptr Un pointeur sur un pointeur de structure
 * \return unsigned int
 **/
unsigned int get_etat_gauche( ptr_str_conf ptr ){
  return ptr->etat_gauche;
}


/**
 * \brief Retourne l'attribut etat_centre.
 * 
 * \param ptr Un pointeur sur un pointeur de structure
 * \return unsigned int
 **/
unsigned int get_etat_centre( ptr_str_conf ptr ){
  return ptr->etat_centre;
}


/**
 * \brief Retourne l'attribut etat_droite.
 * 
 * \param ptr Un pointeur sur un pointeur de structure
 * \return unsigned int 
 **/
unsigned int get_etat_droite( ptr_str_conf ptr ){
  return ptr->etat_droite;
}


/**
 * \brief Retourne l'attribut etat_sortie.
 * 
 * \param ptr Un pointeur sur un pointeur de structure
 * \return unsigned int 
 **/
unsigned int get_etat_sortie( ptr_str_conf ptr ){
  return ptr->etat_sortie;
}


//------------------------------------------------------------//
//                       Autres Fonctions                     //
//------------------------------------------------------------//
/**
 * \brief Compare uniquement les valeurs des états de gauche, du centre et de droite 
 * entre conf1 et conf2.
 * 
 * \param conf1 Un pointeur sur un pointeur de structure configuration
 * \param conf2 Un pointeur sur un pointeur de structure configuration
 * \return true 
 * \return false 
 **/
bool cmp_conf(ptr_str_conf conf1, ptr_str_conf conf2){
  return conf1->etat_gauche == conf2->etat_gauche && conf1->etat_centre == conf2->etat_centre && conf1->etat_droite == conf2->etat_droite;
}


//------------------------------------------------------------//
//                          Affichage                         //
//------------------------------------------------------------//
/**
 * \brief Affiche les attributs d'une configuration
 * 
 * \param ptr Un pointeur sur un pointeur de structure configuration
 **/
void afficher_configuration_elementaire( ptr_str_conf ptr){
  printf("Gauche: %u |", ptr->etat_gauche);
  printf("Centre: %u |", ptr->etat_centre);
  printf("Droite: %u |", ptr->etat_droite);
  printf("Sortie: %u \n", ptr->etat_sortie);
}


/**
 * \brief Affiche une configuration à plus de 2 états
 * 
 * \param ptr Un pointeur sur une structure configuration.
 * \param indice l'indice de la configuration
 **/
void afficher_configuration_multiple( ptr_str_conf ptr, unsigned int indice){
  printf("Indice : %u |", indice);
  printf("Sortie: %u \n", ptr->etat_sortie);
}
