/**
 * \file utils.c
 * \author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * \author Nicolas Zhou (nicolas.zhou@etu.univ-orleans.fr)
 * \brief Des fonctions utiles de traitement pour les autres fonctions
 * \version 0.1
 * \date 2019-10-04
 * 
 * @copyright Copyright (c) 2019
 * 
 **/


#include "utils.h"


//------------------------------------------------------------//
//                       Autres Fonctions                     //
//------------------------------------------------------------//
/**
 * \brief Convertir un nombre decimal entre 0 et 256 en binaire
 * 
 * \param ptr_char Un pointeur sur un tableau de caracteres
 * \param val Un entier à convertir en binaire
 * \return char* la valeur binaire du paramètre
 **/
char* decimal_to_binary(char** ptr_char, unsigned int val){
  char* binary = *ptr_char;

  for (size_t i = 0; i < 8; i++) {
    if ((int)(val / pow(2, 7 - i)) == 1) {
      binary[i] = '1';
      val = val - pow(2, 7 - i);
    }
    else{
      binary[i] = '0';
    }
  }
  return binary;
}


/**
 * \brief Convertit un nombre binaire en decimal
 * 
 * \param binary Un entier a convertir en binaire
 * \return int la valeur binaire du paramètre
 **/
int binary_to_decimal(char* binary){
  int decimal = 0;
  unsigned int taille_val = strlen(binary);
  for (size_t i = 0; i < taille_val; i++) {
    decimal += (binary[i] - '0') * pow(2,  taille_val - 1 - i);
  }
  return decimal;

}
