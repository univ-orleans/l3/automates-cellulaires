/**
 * \file main.c
 * \author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * \author Nicolas Zhou (nicolas.zhou@etu.univ-orleans.fr)
 * \brief L'exécutable du projet
 * \version 0.1
 * \date 2019-10-04
 * 
 * @copyright Copyright (c) 2019
 * 
 **/


#include "automate.h"


/**
 * \brief Le programme principale (l'exécutable)
 * 
 * \param argc Le nombre d'argument passé en paramètre plus le nom du fichier
 * \param argv La liste des arguments passé en paramètre
 * \return int 
 **/
int main(int argc, char const *argv[]) {

  // Vérifiation que l'utilisateur à bien donné un seul argument (le fichier de configuration)
  if( argc != 2 ){
    fprintf(stderr, "Nombre d'argument invalide.");
    exit(EXIT_FAILURE);
  }

	srand(time(NULL)); // Pour l'initialisation aléatoire

  ptr_str_fc f_config = creer_fichier_configuration( argv[1] ); // fichier de configuration
  ptr_automate automate = creer_automate(f_config); // automate
  evoluer( automate ); // évolution de l'automate

  // Si le programme crée une image PGM
  if (get_enregistrer_image(f_config) == 1){
    affiche_automate(automate, &_affiche_automate_image);
  }

  // Si l'on souhaite faire un affichage dans le terminal
  // Si le nombre d'état et de 2 (0 et 1)
  else if (get_nb_etat(f_config) == 2){
    affiche_automate(automate, &_affiche_automate_terminal_binaire);
  }

  // Si le nombre d'état est supérieur à 2 (0, 1, 2, 3, ...)
  else{
    affiche_automate(automate, &_affiche_automate_terminal_somme);
  }


  delete_automate(&automate); // Désalocation de la mémoire de l'automate
  delete_fichier_configuration( f_config ); // Désalocation mémoire des données de configuration

  return 0;
}
