#ifndef AUTOMATE_H
#define AUTOMATE_H

#include "regle.h"
#include "fichier_configuration.h"
#include "image.h"


// Structure de l'automate
typedef struct automate_cell* ptr_automate;

// Constructeur
ptr_automate creer_automate(ptr_str_fc f_config);

// Autres fonctions
void evoluer(ptr_automate automate);

// Destruction
void delete_automate(ptr_automate* ptr);

// Affichage
void _affiche_automate_terminal_binaire(ptr_automate automate);
void _affiche_automate_terminal_somme(ptr_automate automate);
void _affiche_automate_image( ptr_automate automate );
void affiche_automate(ptr_automate automate, void(*_affiche)(ptr_automate));


#endif
