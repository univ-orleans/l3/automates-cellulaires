#ifndef IMAGE_H
#define IMAGE_H

#include "fichier_configuration.h"

#define DOSSIER_IMAGE "img/"
#define FORMAT_IMAGE ".pgm"


// Constructeur
FILE* creer_image( ptr_str_fc f_config );

// Autres fonctions
void ecrire_entete_image( FILE* fichier, ptr_str_fc f_config );
void ecrire_donnees_image_binaire( FILE* fichier, ptr_str_fc f_config, int*** tableau);
void ecrire_donnees_image_ascii( FILE* fichier, ptr_str_fc f_config, int*** tableau);

#endif
