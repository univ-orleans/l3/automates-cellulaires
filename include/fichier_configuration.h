#ifndef FICHIER_CONFIGURATION_H
#define FICHIER_CONFIGURATION_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
// Structure d'un fichier_configurayion
typedef struct fichier_configuration* ptr_str_fc;

// Constructeur
ptr_str_fc creer_fichier_configuration( const char* nom_fichier );

// Accesseurs
char* get_regle( ptr_str_fc f_config );
unsigned int get_nb_etat( ptr_str_fc f_config );
unsigned int get_hauteur( ptr_str_fc f_config );
unsigned int get_largeur( ptr_str_fc f_config );
char get_cellule_symbole( ptr_str_fc f_config );
char* get_emplacement_initial( ptr_str_fc f_config );
bool get_enregistrer_image( ptr_str_fc f_config );
unsigned int get_nb_magique( ptr_str_fc f_config );

// Autres fonctions
void lecture_fichier_configuration(FILE* fichier, ptr_str_fc f_config);

// Destructeur
void delete_fichier_configuration( ptr_str_fc f_config );

// Affichage
void afficher_fichier_configuration( ptr_str_fc f_config );

#endif
