#ifndef REGLE_H
#define REGLE_H

#include "configuration.h"
#include "utils.h"


// Structure d'une regle
typedef struct regle* ptr_str_regle;

// Constructeurs
ptr_str_regle creer_regle(char* numero, unsigned int nombre_etat);
void _config_binaire(char* numero_binaire, unsigned int nombre_etat, ptr_str_regle* ptr);
void _config_somme(char* numero_binaire, unsigned int nombre_etat, ptr_str_regle* ptr);
void creer_tableau_configuration(char* numero_binaire, unsigned int nombre_etat, ptr_str_regle ptr, void(*_config)(char*, unsigned int, ptr_str_regle*));

// Accesseurs
unsigned int get_nombre_etat( ptr_str_regle ptr );
unsigned int get_numero_decimale( ptr_str_regle ptr );
char* get_numero_binaire( ptr_str_regle ptr );
ptr_str_conf* get_tab_configuration( ptr_str_regle ptr );

// Autres fonctions
int recherche_configuration(ptr_str_regle regle, ptr_str_conf conf);
int recherche_indice(ptr_str_regle regle, unsigned int indice);

// Destruction
void delete_regle(ptr_str_regle* regle);

// Affichage
void afficher_regle( ptr_str_regle ptr);


#endif
