#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


// Conversions
char* decimal_to_binary(char** ptr_char, unsigned int val);
int binary_to_decimal(char* binary);

#endif
