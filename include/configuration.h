#ifndef CONOFIGURATION_H
#define CONOFIGURATION_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


// Structure d'une configuration
typedef struct configuration* ptr_str_conf;

// Constructeur
ptr_str_conf creer_configuration();

// Initialisations
void init_configuration_entree(ptr_str_conf ptr, unsigned int gauche, unsigned int centre, unsigned int droite);
void init_configuration_sortie(ptr_str_conf ptr, unsigned int sortie);

// Accesseurs
unsigned int get_etat_gauche( ptr_str_conf ptr );
unsigned int get_etat_centre( ptr_str_conf ptr );
unsigned int get_etat_droite( ptr_str_conf ptr );
unsigned int get_etat_sortie( ptr_str_conf ptr );

// Autres fonctions
bool cmp_conf(ptr_str_conf conf1, ptr_str_conf conf2);

// Affichage
void afficher_configuration_elementaire( ptr_str_conf ptr );
void afficher_configuration_multiple( ptr_str_conf ptr, unsigned int indice);


#endif
