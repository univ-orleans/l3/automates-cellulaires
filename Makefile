# compilateur
CC := gcc
# options de compilation
CFLAGS := -std=c99 -Wall -Wextra -pedantic -ggdb -Wno-unused-but-set-parameter -Wno-unused-variable -Wno-unused-parameter -Wno-abi -iquote "include"

# règle de compilation --- exécutables
all : ./bin/main

./bin/main : ./obj/main.o  ./obj/automate.o ./obj/configuration.o ./obj/utils.o ./obj/regle.o ./obj/fichier_configuration.o ./obj/image.o
	$(CC) $(CFLAGS) -o $@ $^ -lm

./obj/%.o: ./src/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

memoire : ./bin/main
	valgrind --leak-check=full ./bin/main conf/regle_110.ini

# options de compilation
clean:
	rm ./obj/*.o ./bin/main ./img/*

# option d'exécution pour afficher la regle 110 à deux états
run: ./bin/main
	./bin/main conf/regle_110.ini

# option d'exécution pour créer l'imagethe big sierpinski
run_sierpinski: ./bin/main
	./bin/main conf/regle_0100000000.ini

run_aleatoire: ./bin/main
	./bin/main conf/aleatoire.ini
